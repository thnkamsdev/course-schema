<?php
/**
* Plugin Name: Add Course Schema for Yoast
* Description: Plugin to add the Course Schema data to Yoast.
* Version: 1.1
* Author: Mark C. Sherman
* Author URI: https://sherman-design.com/
**/

class CourseSchemaSettings {
	private $course_schema_settings_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'course_schema_settings_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'course_schema_settings_page_init' ) );
	}

	public function course_schema_settings_add_plugin_page() {
		add_menu_page(
			'Course Schema Settings', // page_title
			'Course Schema Settings', // menu_title
			'manage_options', // capability
			'course-schema-settings', // menu_slug
			array( $this, 'course_schema_settings_create_admin_page' ), // function
			'dashicons-admin-generic', // icon_url
			65 // position
		);
	}

	public function course_schema_settings_create_admin_page() {
		$this->course_schema_settings_options = get_option( 'course_schema_settings_option_name' ); ?>

		<div class="wrap">
			<h2>Course Schema Settings</h2>
			<p>Add/edit the Course Title, Description and Dates for each Class.</p>
			<?php settings_errors(); ?>
            
            <style>input.input_date { width: 50%; } input.input_title { width: 750px; } textarea.input_desc { width: 707px; } hr.hr_cs { border: 1px solid grey; max-width: 75%; }</style>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'course_schema_settings_option_group' );
					do_settings_sections( 'course-schema-settings-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function course_schema_settings_page_init() {
		register_setting(
			'course_schema_settings_option_group', // option_group
			'course_schema_settings_option_name', // option_name
			array( $this, 'course_schema_settings_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'course_schema_settings_setting_section', // id
			'Settings', // title
			array( $this, 'course_schema_settings_section_info' ), // callback
			'course-schema-settings-admin' // page
		);

		add_settings_field(
			'emerging_leaders_program_class_title_4_18', // id
			'Emerging Leaders Program', // title
			array( $this, 'emerging_leaders_program_class_title_4_18_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_description_4_19', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_description_4_19_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_start_date_1_0', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_start_date_1_0_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_end_date_1_1', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_end_date_1_1_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_start_date_2_2', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_start_date_2_2_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_end_date_2_3', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_end_date_2_3_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_start_date_3_4', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_start_date_3_4_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'emerging_leaders_program_class_end_date_3_5', // id
			'', // title
			array( $this, 'emerging_leaders_program_class_end_date_3_5_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_title_4_20', // id
			'Executive Leadership Program', // title
			array( $this, 'executive_leadership_program_class_title_4_20_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_description_4_21', // id
			'', // title
			array( $this, 'executive_leadership_program_class_description_4_21_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_start_date_1_6', // id
			'', // title
			array( $this, 'executive_leadership_program_class_start_date_1_6_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_end_date_1_7', // id
			'', // title
			array( $this, 'executive_leadership_program_class_end_date_1_7_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_start_date_2_8', // id
			'', // title
			array( $this, 'executive_leadership_program_class_start_date_2_8_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_end_date_2_9', // id
			'', // title
			array( $this, 'executive_leadership_program_class_end_date_2_9_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_start_date_3_10', // id
			'', // title
			array( $this, 'executive_leadership_program_class_start_date_3_10_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'executive_leadership_program_class_end_date_3_11', // id
			'', // title
			array( $this, 'executive_leadership_program_class_end_date_3_11_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_title_4_22', // id
			'Thrive Program', // title
			array( $this, 'thrive_program_class_title_4_22_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_description_4_23', // id
			'', // title
			array( $this, 'thrive_program_class_description_4_23_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_start_date_1_12', // id
			'', // title
			array( $this, 'thrive_program_class_start_date_1_12_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_end_date_1_13', // id
			'', // title
			array( $this, 'thrive_program_class_end_date_1_13_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_start_date_2_14', // id
			'', // title
			array( $this, 'thrive_program_class_start_date_2_14_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_end_date_2_15', // id
			'', // title
			array( $this, 'thrive_program_class_end_date_2_15_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_start_date_3_16', // id
			'', // title
			array( $this, 'thrive_program_class_start_date_3_16_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);

		add_settings_field(
			'thrive_program_class_end_date_3_17', // id
			'', // title
			array( $this, 'thrive_program_class_end_date_3_17_callback' ), // callback
			'course-schema-settings-admin', // page
			'course_schema_settings_setting_section' // section
		);
	}

	public function course_schema_settings_sanitize($input) {
		$sanitary_values = array();

		// Emerging Leaders Program
		if ( isset( $input['emerging_leaders_program_class_title_4_18'] ) ) {
			$sanitary_values['emerging_leaders_program_class_title_4_18'] = sanitize_text_field( $input['emerging_leaders_program_class_title_4_18'] );
		}

		if ( isset( $input['emerging_leaders_program_class_description_4_19'] ) ) {
			$sanitary_values['emerging_leaders_program_class_description_4_19'] = esc_textarea( $input['emerging_leaders_program_class_description_4_19'] );
		}

		if ( isset( $input['emerging_leaders_program_class_start_date_1_0'] ) ) {
			$sanitary_values['emerging_leaders_program_class_start_date_1_0'] = sanitize_text_field( $input['emerging_leaders_program_class_start_date_1_0'] );
		}

		if ( isset( $input['emerging_leaders_program_class_end_date_1_1'] ) ) {
			$sanitary_values['emerging_leaders_program_class_end_date_1_1'] = sanitize_text_field( $input['emerging_leaders_program_class_end_date_1_1'] );
		}

		if ( isset( $input['emerging_leaders_program_class_start_date_2_2'] ) ) {
			$sanitary_values['emerging_leaders_program_class_start_date_2_2'] = sanitize_text_field( $input['emerging_leaders_program_class_start_date_2_2'] );
		}

		if ( isset( $input['emerging_leaders_program_class_end_date_2_3'] ) ) {
			$sanitary_values['emerging_leaders_program_class_end_date_2_3'] = sanitize_text_field( $input['emerging_leaders_program_class_end_date_2_3'] );
		}

		if ( isset( $input['emerging_leaders_program_class_start_date_3_4'] ) ) {
			$sanitary_values['emerging_leaders_program_class_start_date_3_4'] = sanitize_text_field( $input['emerging_leaders_program_class_start_date_3_4'] );
		}

		if ( isset( $input['emerging_leaders_program_class_end_date_3_5'] ) ) {
			$sanitary_values['emerging_leaders_program_class_end_date_3_5'] = sanitize_text_field( $input['emerging_leaders_program_class_end_date_3_5'] );
		}

		// Executive Leadership Program
		if ( isset( $input['executive_leadership_program_class_title_4_20'] ) ) {
			$sanitary_values['executive_leadership_program_class_title_4_20'] = sanitize_text_field( $input['executive_leadership_program_class_title_4_20'] );
		}

		if ( isset( $input['executive_leadership_program_class_description_4_21'] ) ) {
			$sanitary_values['executive_leadership_program_class_description_4_21'] = esc_textarea( $input['executive_leadership_program_class_description_4_21'] );
		}

		if ( isset( $input['executive_leadership_program_class_start_date_1_6'] ) ) {
			$sanitary_values['executive_leadership_program_class_start_date_1_6'] = sanitize_text_field( $input['executive_leadership_program_class_start_date_1_6'] );
		}

		if ( isset( $input['executive_leadership_program_class_end_date_1_7'] ) ) {
			$sanitary_values['executive_leadership_program_class_end_date_1_7'] = sanitize_text_field( $input['executive_leadership_program_class_end_date_1_7'] );
		}

		if ( isset( $input['executive_leadership_program_class_start_date_2_8'] ) ) {
			$sanitary_values['executive_leadership_program_class_start_date_2_8'] = sanitize_text_field( $input['executive_leadership_program_class_start_date_2_8'] );
		}

		if ( isset( $input['executive_leadership_program_class_end_date_2_9'] ) ) {
			$sanitary_values['executive_leadership_program_class_end_date_2_9'] = sanitize_text_field( $input['executive_leadership_program_class_end_date_2_9'] );
		}

		if ( isset( $input['executive_leadership_program_class_start_date_3_10'] ) ) {
			$sanitary_values['executive_leadership_program_class_start_date_3_10'] = sanitize_text_field( $input['executive_leadership_program_class_start_date_3_10'] );
		}

		if ( isset( $input['executive_leadership_program_class_end_date_3_11'] ) ) {
			$sanitary_values['executive_leadership_program_class_end_date_3_11'] = sanitize_text_field( $input['executive_leadership_program_class_end_date_3_11'] );
		}

		if ( isset( $input['thrive_program_class_title_4_22'] ) ) {
			$sanitary_values['thrive_program_class_title_4_22'] = sanitize_text_field( $input['thrive_program_class_title_4_22'] );
		}

		if ( isset( $input['thrive_program_class_description_4_23'] ) ) {
			$sanitary_values['thrive_program_class_description_4_23'] = esc_textarea( $input['thrive_program_class_description_4_23'] );
		}

		if ( isset( $input['thrive_program_class_start_date_1_12'] ) ) {
			$sanitary_values['thrive_program_class_start_date_1_12'] = sanitize_text_field( $input['thrive_program_class_start_date_1_12'] );
		}

		if ( isset( $input['thrive_program_class_end_date_1_13'] ) ) {
			$sanitary_values['thrive_program_class_end_date_1_13'] = sanitize_text_field( $input['thrive_program_class_end_date_1_13'] );
		}

		if ( isset( $input['thrive_program_class_start_date_2_14'] ) ) {
			$sanitary_values['thrive_program_class_start_date_2_14'] = sanitize_text_field( $input['thrive_program_class_start_date_2_14'] );
		}

		if ( isset( $input['thrive_program_class_end_date_2_15'] ) ) {
			$sanitary_values['thrive_program_class_end_date_2_15'] = sanitize_text_field( $input['thrive_program_class_end_date_2_15'] );
		}

		if ( isset( $input['thrive_program_class_start_date_3_16'] ) ) {
			$sanitary_values['thrive_program_class_start_date_3_16'] = sanitize_text_field( $input['thrive_program_class_start_date_3_16'] );
		}

		if ( isset( $input['thrive_program_class_end_date_3_17'] ) ) {
			$sanitary_values['thrive_program_class_end_date_3_17'] = sanitize_text_field( $input['thrive_program_class_end_date_3_17'] );
		}

		return $sanitary_values;
	}

	public function course_schema_settings_section_info() {
		
	}

	public function emerging_leaders_program_class_title_4_18_callback() {
		printf(
			'<hr class="hr_cs" align="left"><p>&nbsp;</p>Title:&nbsp;&nbsp;<input class="input_title" type="text" name="course_schema_settings_option_name[emerging_leaders_program_class_title_4_18]" id="emerging_leaders_program_class_title_4_18" value="%s">',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_title_4_18'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_title_4_18']) : ''
		);
	}

	public function emerging_leaders_program_class_description_4_19_callback() {
		printf(
			'Description:&nbsp;&nbsp;<textarea class="large-text input_desc" rows="3" name="course_schema_settings_option_name[emerging_leaders_program_class_description_4_19]" id="emerging_leaders_program_class_description_4_19">%s</textarea>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_description_4_19'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_description_4_19']) : ''
		);
	}

	public function emerging_leaders_program_class_start_date_1_0_callback() {
		printf(
			'Class #1 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_start_date_1_0]" id="emerging_leaders_program_class_start_date_1_0" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_1_0'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_1_0']) : ''
		);
	}

	public function emerging_leaders_program_class_end_date_1_1_callback() {
		printf(
			'Class # 1 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_end_date_1_1]" id="emerging_leaders_program_class_end_date_1_1" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_1_1'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_1_1']) : ''
		);
	}

	public function emerging_leaders_program_class_start_date_2_2_callback() {
		printf(
			'Class #2 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_start_date_2_2]" id="emerging_leaders_program_class_start_date_2_2" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_2_2'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_2_2']) : ''
		);
	}

	public function emerging_leaders_program_class_end_date_2_3_callback() {
		printf(
			'Class #2 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_end_date_2_3]" id="emerging_leaders_program_class_end_date_2_3" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_2_3'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_2_3']) : ''
		);
	}

	public function emerging_leaders_program_class_start_date_3_4_callback() {
		printf(
			'Class #3 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_start_date_3_4]" id="emerging_leaders_program_class_start_date_3_4" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_3_4'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_start_date_3_4']) : ''
		);
	}

	public function emerging_leaders_program_class_end_date_3_5_callback() {
		printf(
			'Class #3 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[emerging_leaders_program_class_end_date_3_5]" id="emerging_leaders_program_class_end_date_3_5" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_3_5'] ) ? esc_attr( $this->course_schema_settings_options['emerging_leaders_program_class_end_date_3_5']) : ''
		);
	}

	public function executive_leadership_program_class_title_4_20_callback() {
		printf(
			'<hr class="hr_cs" align="left"><p>&nbsp;</p>Title:&nbsp;&nbsp;<input class="input_title" type="text" name="course_schema_settings_option_name[executive_leadership_program_class_title_4_20]" id="executive_leadership_program_class_title_4_20" value="%s">',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_title_4_20'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_title_4_20']) : ''
		);
	}

	public function executive_leadership_program_class_description_4_21_callback() {
		printf(
			'Description:&nbsp;&nbsp;<textarea class="large-text input_desc" rows="3" name="course_schema_settings_option_name[executive_leadership_program_class_description_4_21]" id="executive_leadership_program_class_description_4_21">%s</textarea>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_description_4_21'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_description_4_21']) : ''
		);
	}

	public function executive_leadership_program_class_start_date_1_6_callback() {
		printf(
			'Class #1 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_start_date_1_6]" id="executive_leadership_program_class_start_date_1_6" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_start_date_1_6'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_start_date_1_6']) : ''
		);
	}

	public function executive_leadership_program_class_end_date_1_7_callback() {
		printf(
			'Class #1 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_end_date_1_7]" id="executive_leadership_program_class_end_date_1_7" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_end_date_1_7'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_end_date_1_7']) : ''
		);
	}

	public function executive_leadership_program_class_start_date_2_8_callback() {
		printf(
			'Class #2 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_start_date_2_8]" id="executive_leadership_program_class_start_date_2_8" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_start_date_2_8'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_start_date_2_8']) : ''
		);
	}

	public function executive_leadership_program_class_end_date_2_9_callback() {
		printf(
			'Class #2 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_end_date_2_9]" id="executive_leadership_program_class_end_date_2_9" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_end_date_2_9'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_end_date_2_9']) : ''
		);
	}

	public function executive_leadership_program_class_start_date_3_10_callback() {
		printf(
			'Class #3 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_start_date_3_10]" id="executive_leadership_program_class_start_date_3_10" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_start_date_3_10'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_start_date_3_10']) : ''
		);
	}

	public function executive_leadership_program_class_end_date_3_11_callback() {
		printf(
			'Class #3 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[executive_leadership_program_class_end_date_3_11]" id="executive_leadership_program_class_end_date_3_11" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['executive_leadership_program_class_end_date_3_11'] ) ? esc_attr( $this->course_schema_settings_options['executive_leadership_program_class_end_date_3_11']) : ''
		);
	}

	public function thrive_program_class_title_4_22_callback() {
		printf(
			'<hr class="hr_cs" align="left"><p>&nbsp;</p>Title:&nbsp;&nbsp;<input class="input_title" type="text" name="course_schema_settings_option_name[thrive_program_class_title_4_22]" id="thrive_program_class_title_4_22" value="%s">',
			isset( $this->course_schema_settings_options['thrive_program_class_title_4_22'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_title_4_22']) : ''
		);
	}

	public function thrive_program_class_description_4_23_callback() {
		printf(
			'Description:&nbsp;&nbsp;<textarea class="large-text input_desc" rows="3" name="course_schema_settings_option_name[thrive_program_class_description_4_23]" id="thrive_program_class_description_4_23">%s</textarea>',
			isset( $this->course_schema_settings_options['thrive_program_class_description_4_23'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_description_4_23']) : ''
		);
	}

	public function thrive_program_class_start_date_1_12_callback() {
		printf(
			'Class #1 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_start_date_1_12]" id="thrive_program_class_start_date_1_12" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['thrive_program_class_start_date_1_12'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_start_date_1_12']) : ''
		);
	}

	public function thrive_program_class_end_date_1_13_callback() {
		printf(
			'Class #1 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_end_date_1_13]" id="thrive_program_class_end_date_1_13" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['thrive_program_class_end_date_1_13'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_end_date_1_13']) : ''
		);
	}

	public function thrive_program_class_start_date_2_14_callback() {
		printf(
			'Class #2 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_start_date_2_14]" id="thrive_program_class_start_date_2_14" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['thrive_program_class_start_date_2_14'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_start_date_2_14']) : ''
		);
	}

	public function thrive_program_class_end_date_2_15_callback() {
		printf(
			'Class #2 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_end_date_2_15]" id="thrive_program_class_end_date_2_15" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['thrive_program_class_end_date_2_15'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_end_date_2_15']) : ''
		);
	}

	public function thrive_program_class_start_date_3_16_callback() {
		printf(
			'Class #3 Start Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_start_date_3_16]" id="thrive_program_class_start_date_3_16" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small>',
			isset( $this->course_schema_settings_options['thrive_program_class_start_date_3_16'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_start_date_3_16']) : ''
		);
	}

	public function thrive_program_class_end_date_3_17_callback() {
		printf(
			'Class #3 End Date:&nbsp;&nbsp;<input class="regular-text.input_date" size="8" type="text" maxlength="10" name="course_schema_settings_option_name[thrive_program_class_end_date_3_17]" id="thrive_program_class_end_date_3_17" value="%s">&nbsp;<small>(format: YYYY-MM-DD)</small><p>&nbsp;</p><hr class="hr_cs" align="left">',
			isset( $this->course_schema_settings_options['thrive_program_class_end_date_3_17'] ) ? esc_attr( $this->course_schema_settings_options['thrive_program_class_end_date_3_17']) : ''
		);
	}

}
if ( is_admin() )
	$course_schema_settings = new CourseSchemaSettings();

function add_new_schema( ) {
	$host = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];

	$domain = 'www.thnk.org/programs/';

	// Note: executive-leadership-program - change $schema_n to 2 once live
	if ( $host == $domain . 'emerging-leaders-program/' ) { $schema_n = 1; } else if ( $host == $domain . 'executive-leadership-program/' ) { $schema_n = 0; } else if ( $host == $domain . 'thrive/' ) { $schema_n = 3; } else { $schema_n = 0; }

    $course_schema_settings_options = get_option( 'course_schema_settings_option_name' );

    // Schema Header
    if ( $schema_n != 0 ) {
    	$schema = '    <script type="application/ld+json">{"@context":"https://schema.org/","@id":"./","@type":"Course",';
    }
    
    // Schema for Emerging Leaders Program
    if ($schema_n == 1) {
    	if ( $course_schema_settings_options['emerging_leaders_program_class_title_4_18'] != '' ) {
    		$schema .= '"name":"' . $course_schema_settings_options['emerging_leaders_program_class_title_4_18'] . '",';
    	} else {
    		$schema .= '"name":"EMERGE: leadership program for leaders in their 1st or second role",'; // default
    	}

    	if ( $course_schema_settings_options['emerging_leaders_program_class_description_4_19'] != '' ) {
    		$schema .= '"description":"' . $course_schema_settings_options['emerging_leaders_program_class_description_4_19'] . '",';
    	} else {
    		$schema .= '"description":"Do you struggle to lead with confidence and authority? Do you feel like you have to learn quickly in your new role but you lack the tools and skills to perform at your best? Joined EMERGE an online leadership program that helps you build confidence, manage conflict more effectively and influence with authority among others.",'; // default
    	}

		if ( $course_schema_settings_options['emerging_leaders_program_class_start_date_1_0'] != '' && $course_schema_settings_options['emerging_leaders_program_class_end_date_1_1'] != '' ) {
			$schema .= '"hasCourseInstance":[{';
			$schema .= '"@type":"CourseInstance","courseMode":"part-time",';
			$schema .= '"endDate":"' . $course_schema_settings_options['emerging_leaders_program_class_end_date_1_1'] . '",';
			$schema .= '"location":"Online",';
			$schema .= '"startDate":"' . $course_schema_settings_options['emerging_leaders_program_class_start_date_1_0'] . '"';

			if ( $course_schema_settings_options['emerging_leaders_program_class_start_date_2_2'] != '' && $course_schema_settings_options['emerging_leaders_program_class_end_date_2_3'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['emerging_leaders_program_class_end_date_2_3'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['emerging_leaders_program_class_start_date_2_2'] . '"';
			}

			if ( $course_schema_settings_options['emerging_leaders_program_class_start_date_3_4'] != '' && $course_schema_settings_options['emerging_leaders_program_class_end_date_3_5'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['emerging_leaders_program_class_end_date_3_5'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['emerging_leaders_program_class_start_date_3_4'] . '"';
			}

			$schema .= '}],';
		}
	}

	// Schema for Executive Leadership Program
	if ($schema_n == 2) {
		if ( $course_schema_settings_options['executive_leadership_program_class_title_4_20'] != '' ) {
    		$schema .= '"name":"' . $course_schema_settings_options['executive_leadership_program_class_title_4_20'] . '",';
    	} else {
    		$schema .= '"name":"EXECUTIVE: leadership program for experienced leaders.",'; // default
    	}

    	if ( $course_schema_settings_options['executive_leadership_program_class_description_4_21'] != '' ) {
    		$schema .= '"description":"' . $course_schema_settings_options['executive_leadership_program_class_description_4_21'] . '",';
    	} else {
    		$schema .= '"description":"Executive leadership program for experienced leaders. Offered in a blended format.",'; // default
    	}

		if ( $course_schema_settings_options['executive_leadership_program_class_start_date_1_6'] != '' && $course_schema_settings_options['executive_leadership_program_class_end_date_1_7'] != '' ) {
			$schema .= '"hasCourseInstance":[{';
			$schema .= '"@type":"CourseInstance","courseMode":"part-time",';
			$schema .= '"endDate":"' . $course_schema_settings_options['executive_leadership_program_class_end_date_1_7'] . '",';
			$schema .= '"location":"Online",';
			$schema .= '"startDate":"' . $course_schema_settings_options['executive_leadership_program_class_start_date_1_6'] . '"';

			if ( $course_schema_settings_options['executive_leadership_program_class_start_date_2_8'] != '' && $course_schema_settings_options['executive_leadership_program_class_end_date_2_9'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['executive_leadership_program_class_end_date_2_9'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['executive_leadership_program_class_start_date_2_8'] . '"';
			}

			if ( $course_schema_settings_options['executive_leadership_program_class_start_date_3_10'] != '' && $course_schema_settings_options['executive_leadership_program_class_end_date_3_11'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['executive_leadership_program_class_end_date_3_11'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['executive_leadership_program_class_start_date_3_10'] . '"';
			}

			$schema .= '}],';
		}
	}

	// Schema for Thrive Program
	if ($schema_n == 3) {
		if ( $course_schema_settings_options['thrive_program_class_title_4_22'] != '' ) {
    		$schema .= '"name":"' . $course_schema_settings_options['thrive_program_class_title_4_22'] . '",';
    	} else {
    		$schema .= '"name":"THRIVE: Lead With the World in Mind",'; // default
    	}

    	if ( $course_schema_settings_options['thrive_program_class_description_4_23'] != '' ) {
    		$schema .= '"description":"' . $course_schema_settings_options['thrive_program_class_description_4_23'] . '",';
    	} else {
    		$schema .= '"description":"Do you want to lead through complexity, drive change at scale, build your self-awareness to increase your effectiveness, get more aligned with your purpose, and network with inspiring, high-achieving leaders? THRIVE is designed to help today’s change-making leaders achieve their visions; a selective program for committed leaders who are seeking a deep and immersive learning experience and have visions of a positive and progressive future. THRIVE is for senior leaders looking to navigate their next step as a leader.",'; // default
    	}

		if ( $course_schema_settings_options['thrive_program_class_start_date_1_12'] != '' && $course_schema_settings_options['thrive_program_class_end_date_1_13'] != '' ) {
			$schema .= '"hasCourseInstance":[{';
			$schema .= '"@type":"CourseInstance","courseMode":"part-time",';
			$schema .= '"endDate":"' . $course_schema_settings_options['thrive_program_class_end_date_1_13'] . '",';
			$schema .= '"location":"Online",';
			$schema .= '"startDate":"' . $course_schema_settings_options['thrive_program_class_start_date_1_12'] . '"';

			if ( $course_schema_settings_options['thrive_program_class_start_date_2_14'] != '' && $course_schema_settings_options['thrive_program_class_end_date_2_15'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['thrive_program_class_end_date_2_15'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['thrive_program_class_start_date_2_14'] . '"';
			}

			if ( $course_schema_settings_options['thrive_program_class_start_date_3_16'] != '' && $course_schema_settings_options['thrive_program_class_end_date_3_17'] != '' ) {
				$schema .= '},{"@type":"CourseInstance","courseMode":"part-time",';
				$schema .= '"endDate":"' . $course_schema_settings_options['thrive_program_class_end_date_3_17'] . '",';
				$schema .= '"location":"Online",';
				$schema .= '"startDate":"' . $course_schema_settings_options['thrive_program_class_start_date_3_16'] . '"';
			}

			$schema .= '}],';
		}
	}

	// Schema Footer
	if ( $schema_n != 0 ) {
		$schema .= '"provider":{"@type":"CollegeOrUniversity","name":"THNK School of Creative Leadership","sameAs":"https://www.thnk.org/","url":"https://www.thnk.org/"}}</script>';
    
   		print($schema . PHP_EOL);
    }
};      
add_filter( 'wpseo_json_ld', 'add_new_schema', 10, 0 );